import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database'
import {AuthService} from '../auth.service';
import {ItemsService} from '../items.service';

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items=[];

  constructor(private db:AngularFireDatabase, 
    private authService:AuthService,
  private ItemsService:ItemsService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        if(!user) return;
    this.db.list('/users/'+user.uid+'/items').snapshotChanges().subscribe(
      items => {
        this.items = [];
        items.forEach(
            item=>{
              let y =item.payload.toJSON();
              y["$key"] = item.key;
              this.items.push(y);
            }
          )
 
      }
    )
 
 
  }
    )
  }  }
