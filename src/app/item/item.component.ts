import { Component, OnInit,Input } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent implements OnInit {
 
  @Input() data:any;
  name;
  price;
  key;
 
checkboxFlag: boolean;
  showTheButton=false; //מחיקה
  showDeleteField=false;//כפתורים נוספים


  
checkChange()  {
  this.ItemsService.updateDone(this.key,this.name,this.checkboxFlag);
}
  showButton(){
    if(!this.showDeleteField){ //מחיקה
      this.showTheButton=true;
    }
  }
  hideButton(){
   
    this.showTheButton=false;
  }
  showDelete(){ //אדיט של טודו
    this.showDeleteField=true;
    this.showTheButton=false;
  }
  cancel(){
    this.showDeleteField=false;
   
  }

delete() {
  this.ItemsService.deleteItem(this.key);
}

  constructor(private ItemsService:ItemsService) { }

  ngOnInit() {
    this.name = this.data.name;
    this.price= this.data.price;
    this.key = this.data.$key;
    this.checkboxFlag = this.data.stock;
  }

}
